// `TEST_CASE`s and `SCENARIO`s for normalier/StringNormalizer.h

#include <catch2.h>
#include <string>

#include "util/util.h"

using namespace std;

TEST_CASE("numnorm::util") {
  SECTION("::strToLower") {
    // Empty string
    std::string subject = "";
    numnorm::util::strToLower(subject);
    REQUIRE(subject == "");
    // All caps
    subject = "ABCDEFG";
    numnorm::util::strToLower(subject);
    REQUIRE(subject == "abcdefg");
    // All lowercase
    subject = "abcdefg";
    numnorm::util::strToLower(subject);
    // Mixed upper / lowercase
    subject = "aBcDeFg";
    numnorm::util::strToLower(subject);
    REQUIRE(subject == "abcdefg");
    // Neither upper or lowercase
    subject = "123.@";
    REQUIRE(subject == "123.@");
  }
}
