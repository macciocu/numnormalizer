// `TEST_CASE`s and `SCENARIO`s for normalizer/StringNormalizer.h

#include <catch2.h>
#include <string>

#include "normalizer/StringNormalizer.h"

TEST_CASE("StringNormalizer") {
  StringNormalizer normalizer;

  SECTION("Single word numbers") {
    REQUIRE(normalizer.normalize("one") == "1");
    REQUIRE(normalizer.normalize("eleven") == "11");
    REQUIRE(normalizer.normalize("twenty") == "20");
    REQUIRE(normalizer.normalize("hundred") == "100");
    REQUIRE(normalizer.normalize("thousand") == "1000");
    REQUIRE(normalizer.normalize("million") == "1000000");
    REQUIRE(normalizer.normalize("billion") == "1000000000");
  }

  SECTION("Lower and uppercase mixtures") {
    REQUIRE(normalizer.normalize("twenty three") == "23");
    REQUIRE(normalizer.normalize("TWENTY THREE") == "23");
    REQUIRE(normalizer.normalize("Twenty three") == "23");
    REQUIRE(normalizer.normalize("twenty Three") == "23");
  }

  // A "multiplier sequence" is for example `hundred thousand = 100 * 1000`.
  SECTION("Mutliplier sequences") {
    REQUIRE(normalizer.normalize("hundred thousand") == "100000");
    REQUIRE(normalizer.normalize("one billion") == "1000000000");
  }

  // "Fake multiplier sequences" are multiplier candidates (e.g. 100, 1000),
  // which appear in decreasing order (e.g. 1000, 100) and therefore are
  // not considered a multiplier; E.g. `thousand hundred = 1000 + 100`.
  SECTION("Fake mutliplier sequences") {
    REQUIRE(normalizer.normalize("thousand hundred") == "1100");
    REQUIRE(normalizer.normalize("billion thousand hundred") == "1000001100");
  }

  SECTION("Additions") {
    REQUIRE(normalizer.normalize("hundred and one") == "101");
    REQUIRE(normalizer.normalize("hundred and twenty one") == "121");
    REQUIRE(normalizer.normalize("three hundred and twenty one") == "321");
  }

  SECTION("Mutliplier sequences with additions") {
    std::string target = "3002";
    std::string subject = "three thousand two";
    REQUIRE(normalizer.normalize(subject) == target);
    subject = "three thousand and two";
    REQUIRE(normalizer.normalize(subject) == target);

    target = "3456";
    subject = "three thousand four hundred fifty six";
    REQUIRE(normalizer.normalize(subject) == target);
    subject = "three thousand and four hundred fifty six";
    REQUIRE(normalizer.normalize(subject) == target);

    target = "200321";
    subject = "two hundred thousand three hundred twenty one";
    REQUIRE(normalizer.normalize(subject) == target);
    subject = "two hundred thousand three hundred and twenty one";
    REQUIRE(normalizer.normalize(subject) == target);
    subject = "two hundred thousand and three hundred and twenty one";
    REQUIRE(normalizer.normalize(subject) == target);

    target = "297321";
    subject = "two hundred thousand ninety seven thousand and three hundred "
              "and twenty one";
    REQUIRE(normalizer.normalize(subject) == target);
    subject = "two hundred thousand and ninety seven thousand and three "
              "hundred and twenty one";
    REQUIRE(normalizer.normalize(subject) == target);
  }

  SECTION("Fake multiplier sequences with additions") {
    REQUIRE(normalizer.normalize("three thousand hundred and two") == "3102");
  }

  SECTION("Number sequences") {
    REQUIRE(normalizer.normalize("one, two, three") == "1, 2, 3");
    std::string subject = "one thousand and one, two hundred, twenty three,";
    std::string target = "1001, 200, 23,";
    REQUIRE(normalizer.normalize(subject) == target);
  }

  SECTION("Multiple numbers within large sentence") {
    std::string subject = "x1 x2 x3 one thousand and one, x1 x2 two hundred x1 "
                          "x2 twenty three x1,";
    std::string target = "x1 x2 x3 1001, x1 x2 200 x1 x2 23 x1,";
    REQUIRE(normalizer.normalize(subject) == target);
  }

  SECTION("The bingo test; Confusing addition and comma occurences") {
    std::string subject = "... and, one and two and three and one hundred two, "
                          "and, one hundred and thirteen and fifty, and seven, "
                          "and sixty three and ninety and nine, and Bingo!";
    std::string target = "... and, 1 and 2 and 3 and 102, and, 113 and 50, and "
                         "7, and 63 and 90 and 9, and Bingo!";
    REQUIRE(normalizer.normalize(subject) == target);
  }
}
