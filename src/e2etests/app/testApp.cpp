// `TEST_CASE`s and `SCENARIO`s for normalier/StringNormalizer.h

#include <string>

#include <iostream>
#include <sstream>

#include <catch2.h>

#include "app/app.h"
#include "util/util.h"

using namespace std;

TEST_CASE("Invalid CLI argument") {
  SECTION("Invalid argument") {
    int argc = 2;
    const char *argv[2] = {"app.cpp", "-x"};
    int rc = numnorm::app::app(argc, (char **)argv);
    REQUIRE(rc != 0);
  }
}

TEST_CASE("Valid CLI arguments") {
  string arg = GENERATE(
      string("-h"), string("--help"), string("-s"), string("--string"));

  SECTION("Valid argument") {
    int argc = 2;
    const char *argv[2] = {"app.cpp", arg.c_str()};
    int rc = numnorm::app::app(argc, (char **)argv);
    REQUIRE(rc == 0);
  }
}

// NB: This test case only validates one normalization use case, basically
// this is a similar use case as the "The bingo test; Confusing addition and
// comma occurences" which we can find in `testStringNormalize.cpp`, goal
// here is to simply validate with our e2e that the application behaves correct
// from start to finish, to validate specific normalization corner use cases
// we use unit testing.
TEST_CASE("Single string normalization") {
  SECTION("Simple use-case") {
    int argc = 3;
    const char *argv[3] = {"app.cpp", "-s",
        "... and, one and two and three and one hundred two, "
        "and, one hundred and thirteen and fifty, and seven, "
        "and sixty three and ninety and nine, and Bingo!"};

    // Redirect cout.
    streambuf *oldCoutStreamBuf = cout.rdbuf();
    ostringstream strCout;
    cout.rdbuf(strCout.rdbuf());

    int rc = numnorm::app::app(argc, (char **)argv);
    REQUIRE(rc == 0);

    // Restore old cout.
    cout.rdbuf(oldCoutStreamBuf);

    string result = strCout.str();
    ostringstream expected;
    expected << "... and, 1 and 2 and 3 and 102, and, 113 and 50, and "
                "7, and 63 and 90 and 9, and Bingo!"
             << endl;

    REQUIRE(result == expected.str());
  }
}
