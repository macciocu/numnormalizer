#include <iostream>
#include <list>
#include <numeric>
#include <sstream>
#include <unordered_map>
#include <unordered_set>
#include <vector>

#include "normalizer/StringNormalizer.h"
#include "util/util.h"

using namespace std;

string StringNormalizer::normalize(string subject) {
  // In some cases part of a number, e.g.: `one hundred and one`.
  const string addition = "and";
  // clang-format off
  // Maps numbers writen as words to corresponding number.
  const unordered_map<string, unsigned int> numbers(
    {{"one", 1},   {"eleven", 11},    {"thirty", 30}, {"hundred", 100},
     {"two", 2},   {"twelve", 12},    {"forty", 40},  {"thousand", 1000},
     {"three", 3}, {"thirteen", 13},  {"fifty", 50},  {"million", 1000000},
     {"four", 4},  {"fourteen", 14},  {"sixty", 60},  {"billion", 1000000000},
     {"five", 5},  {"fifteen", 15},   {"seventy", 70},
     {"six", 6},   {"sixteen", 16},   {"eighty", 80},
     {"seven", 7}, {"seventeen", 17}, {"ninety", 90},
     {"eight", 8}, {"eighteen", 18},
     {"nine", 9},  {"nineteen", 19},
     {"ten", 10},  {"twenty", 20}});
  // clang-format on

  // Inputstream from which we read the text input
  istringstream iss(subject);
  // Outputsream where normalized text is written
  ostringstream oss;
  // Stores number sequences for further processing
  vector<unsigned int> nums;
  // Flag which indicates that at previous iteration, the number sequence ended
  // with "and" or "and,"
  bool additionCandidateAtPrevIter = false;
  // Flag which indicates that at current iteration, the word ends with ","
  bool trailingComma = false;
  // Flag which indicates that at current iteration, word number normalization
  // has been applied.
  bool numberNormalized = false;
  // Counts the number of number normalizations that have been applied.
  unsigned int nNumNorms = 0;
  // Helper variable to track addition candidates. E.g.:
  // "hundred and two" -> two is an addition candidate (true positive)
  // "hundred and thousand" -> thousand is an addition candidate (false
  // positive)
  string additionCandidate = "";

  for (string s; iss >> s;) {
    // save copy of original
    const string origin = s;

    // Normalize so we can use `s` for comparisons
    numnorm::util::strToLower(s);
    trailingComma = s.back() == ',';
    if (trailingComma)
      s.pop_back();

    if (numbers.find(s) != numbers.end()) {
      nums.push_back(((unordered_map<string, unsigned int>)(numbers))[s]);

      if (additionCandidateAtPrevIter) {
        additionCandidateAtPrevIter = false;
        unsigned int v = nums.back();
        unsigned int vPrev = nums[nums.size() - 2];

        // Tackle confusing addition sequences, e.g.:
        // one and two, twenty and one -> 1 and 2, 20 and 1
        // twenty and one -> 20 and 1
        // hundred and one -> 101
        // three thousand and four hundred fifty six -> 3456
        if (v > vPrev || vPrev < 100) {
          // seperate numbers detected
          nums.pop_back();
          oss << computeNum(nums) << " " << additionCandidate << " ";
          nums.clear();
          nums.push_back(v);
          numberNormalized = true;
        }
      }

      if (trailingComma) {
        // Indicates endpoint of number
        // E.g. `one hundred, twenty one` are considered two different numbers
        oss << computeNum(nums) << ", ";
        numberNormalized = true;
        nums.clear();
      }

    } else if (nums.size() && s == addition) {
      additionCandidate = origin;
      additionCandidateAtPrevIter = true;

    } else if (additionCandidate.size()) {
      // "and" detected at end of word number.
      // E.g.: "On black I will bet hundred and I will also bet one on red."
      oss << additionCandidate << " ";
      additionCandidate = "";
      additionCandidateAtPrevIter = false;

    } else if (nums.size()) {
      oss << computeNum(nums) << " " << origin << " ";
      numberNormalized = true;
      nums.clear();

    } else {
      oss << origin << " ";
    }

    if (numberNormalized) {
      numberNormalized = false;
      additionCandidate = "";
      additionCandidateAtPrevIter = false;
      nNumNorms++;
    }
  }

  if (nums.size()) {
    oss << computeNum(nums);
    if (trailingComma) {
      oss << ",";
    }
    oss << " ";
    nNumNorms++;
  }

  string result = oss.str();
  if (nNumNorms) {
    result.pop_back(); // remove last space
  }
  return result;
}

unsigned int StringNormalizer::computeNum(
    const vector<unsigned int> &nums) const {

  if (nums.size() == 1) {
    return nums[0];
  }

  // Holds written number info
  struct Node {
    Node(unsigned int val, bool multiplier = false)
        : val(val), multiplier(multiplier) {}

    // Numeric value of related word number
    // E.g. hundred = 100
    unsigned int val;
    // Indicates if value is a multiplier
    // E.g. three thousand and four hundred fifty six
    // -> thousand and hundred are multipliers
    bool multiplier;
  };

  // Populate nodes
  list<Node> nodes;
  for (auto const &v : nums) {
    if (nodes.empty()) {
      nodes.push_back(Node(v));

    } else if (isMultiplierCandidate(v)) {
      Node &prevNode = nodes.back();

      if (isMultiplierCandidate(prevNode.val)) {
        if (prevNode.val > v) {
          // E.g. one thousand hundred (1 * 1000 + 100)
          nodes.push_back(Node(v));
        } else {
          // Merge consecutive multiplier
          // E.g. one hundred thousand (1 * 100 * 1000 )
          prevNode.val *= v;
        }
      } else {
        // E.g. one hundred (1 * 100)
        nodes.push_back(Node(v, true));
      }

    } else {
      nodes.push_back(Node(v));
    }
  }

  unsigned int num = 0;
  unsigned int sum = 0;
  for (auto const &node : nodes) {
    if (node.multiplier) {
      sum += num * node.val;
      num = 0;
    } else {
      num += node.val;
    }
  }
  return sum + num;
}

bool StringNormalizer::isMultiplierCandidate(const unsigned int subject) const {
  const unordered_set<unsigned int> multipliers(
      {{100, 1000, 1000000, 1000000000}});
  return multipliers.find(subject) != multipliers.end();
}
