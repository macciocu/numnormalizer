#pragma once

#include <string>
#include <vector>

class StringNormalizer {
public:
  std::string normalize(std::string subject);

private:
  /// @param nums String extracted numbers, e.g. "one hundred" -> {1, 100}
  /// @return @a nums converted to number.
  unsigned int computeNum(const std::vector<unsigned int> &nums) const;

  /// Detects multiplier candidate. E.g.: hundred is considered a multiplier
  /// candidate because `two hundred = 2 * 100`.
  /// @param subject Subject for multiplier candidate detection.
  /// @return true if @a subject is multiplier candidate.
  bool isMultiplierCandidate(const unsigned int subject) const;
};
