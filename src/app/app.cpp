#include <iostream>
#include <string>

#include <cstring>
#include <iostream>
#include <string>
#include <vector>

#include "normalizer/StringNormalizer.h"

using namespace std;

namespace numnorm {
namespace app {

void normalize(vector<string> targets);

int app(int argc, char *argv[]) {
  // return code
  int rc = 0;
  // Modes in which the program can be executed.
  enum Mode {
    UNKNOWN,
    HELP,
    STRING,
  };
  // Determined in function of given CLI args.
  Mode mode = UNKNOWN;
  // Holds sources which are target for number normalization.
  vector<string> sources;

  // Skip the program name.
  argc--;
  argv++;

  while (argc) {
    if (mode == UNKNOWN) {
      if (strcmp(*argv, "-h") == 0 || strcmp(*argv, "--help") == 0) {
        mode = HELP;
        break;
      }
      if (strcmp(*argv, "-s") == 0 || strcmp(*argv, "--string") == 0) {
        mode = STRING;
      }
    } else if (mode == STRING) {
      sources.push_back(string(*argv));
    }
    argc--;
    argv++;
  }

  switch (mode) {
  case UNKNOWN:
    rc = 1;
    cout << "[ERROR] CLI Argument not supported." << endl;
    cout << "Execute with \"-h\" to display help." << endl;
    break;
  case HELP:
    cout << "DESCRIPTION:" << endl
         << "This CLI application normalizes numbers in strings." << endl
         << "It convert written numbers in words to digits. The to be" << endl
         << "converted text is provided on the command line, the" << endl
         << "normalized text is printed to stdout." << endl
         << endl
         << "USAGE:" << endl
         << "- Single string normalization:" << endl
         << "  $ numnormalizer -s <string>" << endl
         << "  $ numnormalizer --string <string>" << endl
         << "- Multi string normalization:" << endl
         << "  $ numnormalizer -s \"<string1>\" \"<string2>\"" << endl
         << endl
         << "KNOWN LIMITATIONS:"
         << " - The max. number to be normalized is system dependent," << endl
         << "   it will equal 4294967295 on most systems (= the max " << endl
         << "   value of an 32 bit unsigned int)." << endl
         << " - A comma ',' always indicates a number endpoint, e.g.:" << endl
         << "  \"hundred, two, hundred\" are considered 3 separate " << endl
         << "  and will be normalized as \"100, 2, 100\"." << endl;
    break;
  case STRING:
    normalize(sources);
    break;
  }

  return rc;
}

void normalize(vector<string> targets) {
  for (auto target : targets) {
    StringNormalizer normalizer;
    target = normalizer.normalize(target);
    cout << target << endl;
  }
}

} // namespace app
} // namespace numnorm
