#include <algorithm>

#include "util/util.h"

namespace numnorm {
namespace util {

void strToLower(std::string &s) {
  transform(s.begin(), s.end(), s.begin(),
      [](unsigned char c) { return tolower(c); });
}

} // namespace util
} // namespace numnorm
