#pragma once

#include <string>

namespace numnorm {
namespace util {

/// Convert @a s to lowercase.
void strToLower(std::string &s);

} // namespace util
} // namespace numnorm
